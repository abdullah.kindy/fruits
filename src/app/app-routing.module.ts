import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('../domains/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'fruits',
    loadChildren: () => import('../domains/fruits/fruits.module').then(m => m.FruitsModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
