import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { Fruit } from '../_entities/_models/fruit.model';

import { FruitsDataService } from './fruits-data.service';

describe('FruitsDataService', () => {
  let testUnit: FruitsDataService;
  let testScheduler: TestScheduler;

  const httpClientMock = {get: jasmine.createSpy()};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: HttpClient, useValue: httpClientMock }
      ]
    });
    testUnit = TestBed.inject(FruitsDataService);
    testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  it('should be created', () => {
    expect(testUnit).toBeTruthy();
  });

  describe('loadFruits$()', () => {
    describe('when httpCleint.get() returns undefined', () => {
      it('should return empty list', () => {
        testScheduler.run(({ expectObservable, cold }) => {
          httpClientMock.get.and.returnValue(cold('a', { a: undefined }));
          expectObservable(testUnit.loadFruits$()).toBe('a', { a: [] });
        });
      });
    });

    describe('when httpCleint.get() returns data', () => {
      it('should return data list', () => {
        testScheduler.run(({ expectObservable, cold }) => {
          const expectedResult = [new Fruit(), new Fruit()]
          httpClientMock.get.and.returnValue(cold('a', { a: [undefined, undefined] }));
          expectObservable(testUnit.loadFruits$()).toBe('a', { a: expectedResult });
        });
      });
    });
  })
});
