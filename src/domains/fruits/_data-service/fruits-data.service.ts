import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map as _map } from 'lodash/fp'
import { map, Observable } from 'rxjs';
import { Fruit } from '../_entities/_models/fruit.model';
@Injectable({
  providedIn: 'root'
})
export class FruitsDataService {
  baseUrl: string = '/api/fruit'
  constructor(private readonly httpClient: HttpClient) { }

  loadFruits$(): Observable<Fruit[]> {
    return this.httpClient.get<Fruit[]>(`${this.baseUrl}/all`).pipe(
      map(_map((fruit) => new Fruit(fruit)))
    );
  }
}
