import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FruitsListComponent } from './fruits-list/fruits-list.component';
import { FruitsRoutingModule } from './fruits-routing.module';
import { FruitsStateModule } from './_state/state.module';
import { MatIconModule } from '@angular/material/icon'
import { MatTableModule } from '@angular/material/table'
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { NutritionsComponent } from './nutritions/nutritions.component'


@NgModule({
  declarations: [
    FruitsListComponent,
    NutritionsComponent,
  ],
  imports: [
    CommonModule,
    FruitsRoutingModule,
    FruitsStateModule,
    MatIconModule,
    MatTableModule,
    MatTooltipModule,
    MatDialogModule,
    MatCardModule
  ]
})
export class FruitsModule { }
