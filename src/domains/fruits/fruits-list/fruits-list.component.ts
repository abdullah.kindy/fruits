import { Component } from '@angular/core';
import { FruitsAppService } from '../_app-service/fruits-app.service';
import { MatDialog } from '@angular/material/dialog';
import { NutritionsComponent } from '../nutritions/nutritions.component';
@Component({
  selector: 'app-fruits-list',
  templateUrl: './fruits-list.component.html',
})
export class FruitsListComponent {
  displayedColumns: string[] = ['name', 'genus', 'family', 'order', 'nutritions'];
  nutritionsComponent = NutritionsComponent;

  constructor( readonly appService: FruitsAppService, readonly dialog: MatDialog ) { }
}
