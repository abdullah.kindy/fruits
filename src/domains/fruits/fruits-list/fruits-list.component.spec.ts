import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { of } from 'rxjs';
import { FruitsAppService } from '../_app-service/fruits-app.service';
import { provideMockStore } from '@ngrx/store/testing';

import { FruitsListComponent } from './fruits-list.component';
import { MatIconModule } from '@angular/material/icon';

describe('FruitsListComponent', () => {
  let component: FruitsListComponent;
  let fixture: ComponentFixture<FruitsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FruitsListComponent ],
      providers: [
        provideMockStore(),
        {provide: FruitsAppService, usevalue: {fruits$: of([])}},
        {provide: MatDialog, useValue: {open: jasmine.createSpy()}}
      ],
      imports: [
        MatIconModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FruitsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
