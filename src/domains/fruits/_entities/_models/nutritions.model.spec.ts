import { Nutritions } from "./nutritions.model"

describe('Nutritions', () => {
    const emptyObject: Nutritions = {
        carbohydrates: undefined!,
        protein: undefined!,
        fat: undefined!,
        calories: undefined!,
        sugar: undefined!
    }

    const fullObject: Nutritions = {
        carbohydrates: 1,
        protein: 2,
        fat: 3,
        calories: 4,
        sugar: 5
    }

    describe('when undefined is passed', () => {
        it('should return an empty nutritions object', () => {
            const result = new Nutritions();

            expect(result).toEqual(jasmine.objectContaining(emptyObject));
        })
    })

    describe('when empty is passed', () => {
        it('should return an empty nutritions object', () => {
            const result = new Nutritions({});

            expect(result).toEqual(jasmine.objectContaining(emptyObject));
        })
    })

    describe('when value is passed', () => {
        it('should return a full nutritions object', () => {
            const result = new Nutritions(fullObject);

            expect(result).toEqual(jasmine.objectContaining(fullObject));
        })
    })
})