import { Nutritions } from "./nutritions.model";

export class Fruit {
    id: number;
    genus: string;
    name: string;
    family: string;
    order: string;
    nutritions: Nutritions; 

    constructor(data: any = {}) {
        const { id = undefined, genus = undefined, name = undefined, family = undefined, order = undefined, nutritions = undefined } = data;

        this.id = id;
        this.genus = genus;
        this.name = name;
        this.family = family;
        this.order = order;
        this.nutritions = new Nutritions(nutritions);
    }
}