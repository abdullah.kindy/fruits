export class Nutritions {
    carbohydrates: number;
    protein: number;
    fat: number;
    calories: number;
    sugar: number;

    constructor(data: any = {}) {
        const {carbohydrates = undefined, protein = undefined, fat = undefined, calories = undefined, sugar = undefined} = data;

        this.carbohydrates = carbohydrates;
        this.protein = protein;
        this.fat = fat;
        this.calories = calories;
        this.sugar = sugar;
    }
}