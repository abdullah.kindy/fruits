import { Fruit } from "./fruit.model"
import { Nutritions } from "./nutritions.model"

describe('Fruit', () => {
    const emptyObject: Fruit = {
        id: undefined!,
        genus: undefined!,
        name: undefined!,
        family: undefined!,
        order: undefined!,
        nutritions: new Nutritions() 
    }

    const fullObject: Fruit = {
        id: 1,
        genus: 'some genus',
        name: 'some name',
        family: 'some family',
        order: 'some oreder',
        nutritions: new Nutritions({fat: 0.4}) 
    }

    describe('when undefined is passed', () => {
        it('should return an empty fruit', () => {
            const result = new Fruit();

            expect(result).toEqual(jasmine.objectContaining(emptyObject));
        })
    })

    describe('when empty is passed', () => {
        it('should return an empty fruit', () => {
            const result = new Fruit({});

            expect(result).toEqual(jasmine.objectContaining(emptyObject));
        })
    })

    describe('when value is passed', () => {
        it('should return an full fruit', () => {
            const result = new Fruit(fullObject);

            expect(result).toEqual(jasmine.objectContaining(fullObject));
        })
    })
})