import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Fruit } from '../_entities/_models/fruit.model';
import { fruitsSelector } from '../_state/fruits/fruits.selectors';

@Injectable({
  providedIn: 'root'
})
export class FruitsAppService {
  fruits$: Observable<Fruit[]> = this.store.select(fruitsSelector);

  constructor(private readonly store: Store) { }
}
