import { Component, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Fruit } from '../_entities/_models/fruit.model';

@Component({
  selector: 'app-nutritions',
  templateUrl: './nutritions.component.html',
})
export class NutritionsComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public fruit: Fruit
 ) { }
}
