import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Fruit } from '../_entities/_models/fruit.model';

import { NutritionsComponent } from './nutritions.component';

describe('NutritionsComponent', () => {
  let component: NutritionsComponent;
  let fixture: ComponentFixture<NutritionsComponent>;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NutritionsComponent ],
      providers: [{provide: MAT_DIALOG_DATA, useValue: new Fruit()}],
      imports: [MatCardModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
