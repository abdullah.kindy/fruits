import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FruitsEffects } from './fruits/fruits.effects';
import { fruitsFeatureKey } from './fruits/fruits.entities';
import { fruitsReducer } from './fruits/fruits.reducer';


@NgModule({
  imports: [
    EffectsModule.forFeature([FruitsEffects]),
    StoreModule.forFeature(fruitsFeatureKey, fruitsReducer),
    ]
})
export class FruitsStateModule { }
