import { EntityState } from '@ngrx/entity';
import { Fruit } from '../../_entities/_models/fruit.model';

export interface FruitsFeatureState extends EntityState<Fruit> {}

export const fruitsFeatureKey = 'fruitsFeature';

export enum fruitsAction {
  LoadFruits = '[FruitsFeature] load fruits',
  LoadFruitsSuccess = '[FruitsFeature] load fruits success',
  LoadFruitsFail = '[FruitsFeature] load fruits success fail',
}
