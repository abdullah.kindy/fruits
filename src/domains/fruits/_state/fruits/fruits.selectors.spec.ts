import { Fruit } from "../../_entities/_models/fruit.model";
import { FruitsFeatureState } from "./fruits.entities";
import { fruitsSelector } from "./fruits.selectors";

describe('FruitsSelectors', () => {
    const fruit1 = new Fruit({id: 1});
    const fruit2 = new Fruit({id: 2});
    const state: FruitsFeatureState = { ids: [1, 2], entities: {1: fruit1, 2: fruit2}};

    describe('fruitsSelector', () => {
        it('should return the fruits as list', () => {
            expect(fruitsSelector.projector(state)).toEqual([fruit1, fruit2]);
        })
    })
})