import { Fruit } from "../../_entities/_models/fruit.model";
import { loadFruitsSuccess } from "./fruits.actions";
import { FruitsFeatureState } from "./fruits.entities";
import { fruitsReducer, initialFruitsState } from "./fruits.reducer";

describe('FruitsReducer', () => {
  it('should have initial state', () => {
    const expected: FruitsFeatureState = { ids: [], entities: {}};
    expect(fruitsReducer(initialFruitsState, {} as any)).toEqual(expected);
  });

  describe('loadFruitsSuccess', () => {
    it('should update the entities', () => {
      const fruit1 = new Fruit({id: 1});
      const fruit2 = new Fruit({id: 2});
      const expected: FruitsFeatureState = { ids: [1, 2], entities: {1: fruit1, 2: fruit2}};
      const action = loadFruitsSuccess({fruits: [fruit1, fruit2]});
      expect(fruitsReducer(initialFruitsState, action)).toEqual(expected);
    });
  })
});