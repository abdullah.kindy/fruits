import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jasmine-marbles';
import { Observable } from 'rxjs';
import { FruitsDataService } from '../../_data-service/fruits-data.service';
import { loadFruits, loadFruitsFail, loadFruitsSuccess } from './fruits.actions';
import { FruitsEffects } from './fruits.effects';


describe('FruitsEffects', () => {
  let testUnit: FruitsEffects;
  let actions: Observable<any>;

  const dataServiceMock = {
    loadFruits$: jasmine.createSpy() 
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FruitsEffects,
        provideMockActions(() => actions),
        {
          provide: FruitsDataService, useValue: dataServiceMock
        }
      ],
    });

    testUnit = TestBed.get(FruitsEffects);
  });

  describe('loadFruits$', () => {
    it('should work', () => {
      const action = loadFruits();
      const completion = loadFruitsSuccess({fruits: []});
      
      dataServiceMock.loadFruits$.and.returnValue(cold('b', { b: [] }));

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });
  
      expect(testUnit.loadFruits$).toBeObservable(expected);
    });

    it('should fail', () => {
      const action = loadFruits();
      const completion = loadFruitsFail({error: 'error'});
  
      dataServiceMock.loadFruits$.and.returnValue(cold('#'));

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });
  
      expect(testUnit.loadFruits$).toBeObservable(expected);
    });
  })

  describe('ngrxOnInitEffects()', () => {
    it('should initially loadFruits', () => {
      expect(testUnit.ngrxOnInitEffects()).toEqual(loadFruits())
    })
  })
});