import { Action, createReducer, on } from '@ngrx/store';
import { loadFruitsSuccess } from './fruits.actions';
import { fruitsAdapter } from './fruits.adapter';
import { FruitsFeatureState } from './fruits.entities';

export const initialFruitsState: FruitsFeatureState = fruitsAdapter.getInitialState({});

const reducer = createReducer(
  initialFruitsState,
  on(loadFruitsSuccess, (state, { fruits }) =>
    fruitsAdapter.addMany(fruits, state)
  )
);

// tslint:disable-next-line: typedef
export function fruitsReducer(
  state: FruitsFeatureState | undefined,
  action: Action
) {
  return reducer(state, action);
}
