import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { Fruit } from '../../_entities/_models/fruit.model';

export const fruitsAdapter: EntityAdapter<Fruit> =
  createEntityAdapter<Fruit>({
    selectId: ({ id }: Fruit) => id,
  });
