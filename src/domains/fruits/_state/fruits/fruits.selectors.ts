import { createFeatureSelector, createSelector } from '@ngrx/store';
import { get as _get } from 'lodash';
import { fruitsAdapter } from './fruits.adapter';
import { fruitsFeatureKey, FruitsFeatureState } from './fruits.entities';

const { selectAll } = fruitsAdapter.getSelectors();

const fruitsFeatureState =
  createFeatureSelector<FruitsFeatureState>(fruitsFeatureKey);

export const fruitsSelector = createSelector(
  fruitsFeatureState,
  selectAll
);
