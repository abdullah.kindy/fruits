import { createAction, props } from '@ngrx/store';
import { Fruit } from '../../_entities/_models/fruit.model';
import { fruitsAction } from './fruits.entities';

export const loadFruits = createAction(fruitsAction.LoadFruits);

export const loadFruitsSuccess = createAction(
  fruitsAction.LoadFruitsSuccess,
  props<{ fruits: Fruit[] }>()
);

export const loadFruitsFail = createAction(
  fruitsAction.LoadFruitsFail,
  props<{ error: string }>()
);
